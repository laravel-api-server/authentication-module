<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use ApiServer\Core\Models\Option;

class AddInitialAuthConfigValues extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Option::create([
            'key' => "serverAuthTokenTTL",
            'value' => "172800"
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Option::where('key', '=', 'serverAuthTokenTTL')->delete();
    }
}
