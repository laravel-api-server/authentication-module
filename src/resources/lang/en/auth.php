<?php

return [
    'jwt_token_expired' => 'Auth token expired.',
    'jwt_token_invalid' => 'Auth token invalid.',
    'jwt_user_not_found' => 'Coresponding user for auth token not found.',
];
