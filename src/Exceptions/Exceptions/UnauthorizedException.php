<?php

namespace ApiServer\Authentication\Exceptions\Exceptions;

class UnauthorizedException extends \Exception
{
    protected $code = 401;
}
