<?php

namespace ApiServer\Authentication;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class ModuleServiceProvider extends ServiceProvider
{
    public function register() {
        // register the modules service providers
        $this->app->register('Tymon\JWTAuth\Providers\LaravelServiceProvider');

        // register facades https://stackoverflow.com/a/22749871/6517870
        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('JWTAuth', 'Tymon\JWTAuth\Facades\JWTAuth');
        $loader->alias('JWTFactory', 'Tymon\JWTAuth\Facades\JWTFactory');
    }

    public function boot(Router $router) {
        // boot routes
        require __DIR__ . '/Http/routes.php';

        // boot route middlewares
        $router->aliasMiddleware(
            'auth.jwt',
            'ApiServer\Authentication\Http\Middleware\AuthenticateJwt'
        );

        // load language files
        $this->loadTranslationsFrom(__DIR__.'/resources/lang', 'authentication');

        // boot database migrations
        $this->loadMigrationsFrom(__DIR__.'/database/migrations');
    }
}

?>
