<?php

Route::group([
    'namespace' => '\ApiServer\Authentication\Http\Controllers',
    'middleware' => 'cors'], function()
{
    /**
     * Routes without authentication
     */
    Route::post('auth-token-create', 'AuthTokenController@create');
    Route::post('auth-token-refresh', 'AuthTokenController@refresh');

    //TODO :
    //Route::post('auth-token-decode', 'AuthTokenController@create');
});

?>
