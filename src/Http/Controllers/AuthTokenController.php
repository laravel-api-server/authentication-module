<?php

namespace ApiServer\Authentication\Http\Controllers;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\GoneHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;

use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

use Illuminate\Routing\Controller;

use ApiServer\Core\Exceptions\Exceptions\ValidationException;
use ApiServer\Authentication\Exceptions\Exceptions\UnauthorizedException;
use ApiServer\Core\Models\Option;

class AuthTokenController extends Controller
{
    public function create(Request $request)
    {
        //check if request has valid format
        if(!$request->isJson()) {
            throw new UnsupportedMediaTypeHttpException(trans('api.update_unsupported_media', [
                'modelName' => trans_choice('terms.'.strtolower($modelName), 1)
            ]));
        }

        if(     $request->filled('username')
            &&  $request->filled('password'))
        {
            //if user provided email and password
            $credentials = [
                'name' => $request->input('username'),
                'password' => $request->input('password')
            ];

            try {
                // attempt to verify the credentials and create a token for the user

                $authTokenTTL = config('jwt.ttl');
                try {
                    $authTokenTTL = Option::where(
                        'key', 'serverAuthTokenTokenTTL'
                    )->firstOrFail()->value;
                } catch(\Exception $e) {}

                $customClaims = ['exp' => time()+$authTokenTTL];
                if (! $token = \JWTAuth::customClaims($customClaims)->attempt($credentials)) {
                    throw new UnauthorizedException(trans('api.unauthorized'));
                }

            } catch (JWTException $e) {
                // something went wrong whilst attempting to encode the token
                throw new HttpException(trans('api.jwt_generate_error'));
            }

            // all good so return the token
            return response()->json([
                'data' => [
                    'type' => "auth-token",
                    'attributes' => [
                        'token' => $token
                    ]
                ]
            ])->setStatusCode(201);
        }

        throw new UnauthorizedException(trans('api.unauthorized'));
    }

    public function refresh(Request $request)
    {
        if(!$request->filled('token')) {
            throw new ValidationException(
                null,
                new \MessageBag(['type' => "Attribute token is required."])
            );
        }

        $oldToken = $request->input('token');

        $authTokenTTL = config('jwt.ttl');
        try {
            $authTokenTTL = Option::where(
                'key', 'serverAuthTokenTokenTTL'
            )->firstOrFail()->value;
        } catch(\Exception $e) {}

        $customClaims = ['exp' => time()+$authTokenTTL];
        if (! $newToken = \JWTAuth::setToken($oldToken)->customClaims($customClaims)->refresh()) {
            throw new UnauthorizedException(trans('api.unauthorized'));
        }

        // all good so return the token
        return response()->json([
            'data' => [
                'type' => "auth-token",
                'attributes' => [
                    'token' => $newToken
                ]
            ]
        ])->setStatusCode(201);
    }
}
