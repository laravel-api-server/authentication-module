<?php

/*
 * This file is part of jwt-auth.
 *
 * (c) Sean Tymon <tymon148@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace ApiServer\Authentication\Http\Middleware;

use ApiServer\Authentication\Exceptions\Exceptions\UnauthorizedException;

use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class AuthenticateJwt extends BaseMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        if (! $token = $this->auth->setRequest($request)->getToken()) {
            return $next($request);
        }

        try {
            $user = $this->auth->authenticate($token);
        } catch (TokenExpiredException $e) {
            throw new UnauthorizedException(
                trans('authentication::auth.jwt_token_expired')
            );
        } catch (JWTException $e) {
            throw new UnauthorizedException(
                trans('authentication::auth.jwt_token_invalid')
            );
        }

        if (! $user) {
            throw new UnauthorizedException(
                trans('authentication::auth.jwt_user_not_found')
            );
        }

        return $next($request);
    }
}
